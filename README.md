# README #

### Cosa contiene ###

Questo repository contiene la classe e il pacchetto relativi al progetto 
**Matematica Libera**.

Il tutto è ancora in costruzione e lontano da una versione stabile.

In particolare in questo repository si trova quanto serve per generare:

* matlibera.cls
* matliberamat.sty
* matliberagraf.sty

In entrambe le directory sono presenti i seguenti file:

* <...>.dtx: contiene il sorgente del pacchetto e della documentazione.
* Makefile: per semplificare la compilazione.
* <...>.pdf: la documentazione dei pacchetti.
* <...test>.tex: per il test dei pacchetti e per gli esempi del loro uso.


### Come compilare ###

* make help elenca i possibili argomenti di make.

### Come contribuire ###

Per qualunque proposta di correzione o segnalazione di errori:

* Compilare un issue.
* Scrivere a daniele.zambelli@gmail.com.
