# Makefile per il pacchetto matlibera
# (c) 2022-2024 Daniele Zambelli - daniele.zambelli@gmail.com
# v0.2.0 -- 2023/02/20

NAMECLS = matlibera
NAMEMAT = matliberamat
NAMEGRAF = matliberagraf
TESTCLS = test$(NAMECLS)
TESTMAT = test$(NAMEMAT)
TESTGRAF = test$(NAMEGRAF)

CLSJAXDIRM = htmlm/cls/mathjax
CLSMLDIRM = htmlm/cls/mathml
MATHJAXDIRM = htmlm/mat/mathjax
MATHMLDIRM = htmlm/mat/mathml
GRAFJAXDIRM = htmlm/graf/mathjax
GRAFMLDIRM = htmlm/graf/mathml
CLSJAXDIR = html/cls/mathjax
CLSMLDIR = html/cls/mathml
MATHJAXDIR = html/mat/mathjax
MATHMLDIR = html/mat/mathml
GRAFJAXDIR = html/graf/mathjax
GRAFMLDIR = html/graf/mathml

PACKAGE = matlibera.dtx \
		  matlibera.pdf \
		  README \
		  Makefile

LUALATEX = lualatex --shell-escape

MATHJAXM = "mathjax"
MATHMLM = "mathml"

MATHJAX = "mathjax, 4, sec-filename, fn-in, blind"
MATHML = "mathml, 4, sec-filename, fn-in, blind"

FILE_CLEAN = \
*.aux *.idx *.ilg *.ind *.log *.glo *.gls *.toc *.log *.pyg *~* \
*backup *.out *.listing \
*.tmp *.4ct *4tc *css *.dvi *.html *.idv *.lg *.xref ./*.svg \
*.gnuplot *.table

VERSION	= 2_0
TAR = $(NAMECLS)-$(VERSION).tar.gz 
ZIP = $(NAMECLS)-$(VERSION).zip

# Cambiare la variabile TEXDIR se non si vuole installare nell'albero personale
TEXDIR = `kpsewhich --expand-path='$$TEXMFHOME'`
INSDIR = $(TEXDIR)/tex/latex/$(NAMECLS)
DOCDIR = $(TEXDIR)/doc/latex/$(NAMECLS)
SRCDIR = $(TEXDIR)/source/latex/$(NAMECLS)

.PHONY: help matlibera testdtx \
testcls testclsjm testclsxm testclsj testclsx \
testgraf testgrafjm testgrafxm testgrafj testgrafx \
testmat testmatjm testmatxm testmatj testmatx \
all \
clean cleanall \
dist-tar dist-zip

.SILENT: help

help:
	echo ''
	echo 'Makefile targets:'
	echo ''
	echo 'help        - Questo messaggio'
	echo 'matlibera   - Compila usando lualatex il file $(NAMECLS).dtx e produce:'
	echo '               + la classe: ' $(NAMECLS).cls
	echo '               + il pacchetto: ' $(NAMEMAT).sty
	echo '               + la documentazione: ' $(NAMECLS).pdf
	echo 'testcls     - Produce: ' $(TESTCLS).pdf
	echo 'testclsjm   - Traduce: ' $(TESTCLS).tex in HTML+MATJAX monolitico
	echo 'testclsxm   - Traduce: ' $(TESTCLS).tex in HTML+MATHML monolitico
	echo 'testclsj    - Traduce: ' $(TESTCLS).tex in HTML+MATJAX
	echo 'testclsx    - Traduce: ' $(TESTCLS).tex in HTML+MATHML
	echo 'testgraf    - Produce: ' $(TESTGRAF).pdf
	echo 'testgrafjm  - Traduce: ' $(TESTGRAF).tex in HTML+MATJAX monolitico
	echo 'testgrafxm  - Produce: ' $(TESTGRAF).tex in HTML+MATHML monolitico
	echo 'testgrafj   - Traduce: ' $(TESTGRAF).tex in HTML+MATJAX
	echo 'testgrafx   - Produce: ' $(TESTGRAF).tex in HTML+MATHML
	echo 'testmat     - Produce: ' $(TESTMAT).pdf
	echo 'testmatjm   - Traduce: ' $(TESTMAT).tex in HTML+MATJAX monolitico
	echo 'testmatxm   - Produce: ' $(TESTMAT).tex in HTML+MATHML monolitico
	echo 'testmatj    - Traduce: ' $(TESTMAT).tex in HTML+MATJAX
	echo 'testmatx    - Produce: ' $(TESTMAT).tex in HTML+MATHML
	echo 'all         - Esegue sia la compilazione del dtx sia i test'
	echo 'clean       - Rimuove i file temporanei'
	echo 'cleanall    - Rimuove tutti file creati da TeX'
	echo 'dist-tar    - Crea una distibuzione (.tar.gz) della classe'
	echo 'dist-zip    - Crea una distibuzione (.zip) della classe'
	echo ''

matlibera: $(NAMECLS).dtx
	$(LUALATEX) $<

testcls: $(TESTCLS).tex
	$(LUALATEX) $<
	$(LUALATEX) $<

testclsjm: $(TESTCLS).tex
	make4ht $(TESTCLS).tex -c ml_make4ht -l -u -s -d $(CLSJAXDIRM) $(MATHJAXM)

testclsxm: $(TESTCLS).tex
	make4ht $(TESTCLS).tex -c ml_make4ht -l -u -s -d $(CLSMLDIRM) $(MATHMLM)

testclsj: $(TESTCLS).tex
	make4ht $(TESTCLS).tex -c ml_make4ht -l -u -s -d $(CLSJAXDIR) $(MATHJAX)

testclsx: $(TESTCLS).tex
	make4ht $(TESTCLS).tex -c ml_make4ht -l -u -s -d $(CLSMLDIR) $(MATHML)

testgraf: $(TESTGRAF).tex
	$(LUALATEX) $<
	$(LUALATEX) $<

testgrafjm: $(TESTGRAF).tex
	make4ht $(TESTGRAF).tex -c ml_make4ht -l -u -s -d $(GRAFJAXDIRM) $(MATHJAXM)

testgrafxm: $(TESTGRAF).tex
	make4ht $(TESTGRAF).tex -c ml_make4ht -l -u -s -d $(GRAFMLDIRM) $(MATHMLM)

# testgrafj: $(TESTGRAF).tex
# 	make4ht $(TESTGRAF).tex -c ml_make4ht \
# 	-f html5-common_domfilters \
# 	-l -u -s -d $(GRAFJAXDIR) $(MATHJAX)
# 
# testgrafx: $(TESTGRAF).tex
# 	make4ht $(TESTGRAF).tex -c ml_make4ht -l -u -s -d $(GRAFMLDIR) $(MATHML)

testgrafj: $(TESTGRAF).tex
	make4ht $(TESTGRAF).tex -c ml_make4ht -l -u -s -d $(GRAFJAXDIR) $(MATHJAX)

testgrafx: $(TESTGRAF).tex
	make4ht $(TESTGRAF).tex -c ml_make4ht -l -u -s -d $(GRAFMLDIR) $(MATHML)

testmat: $(TESTMAT).tex
	$(LUALATEX) $<
	$(LUALATEX) $<

testmatjm: $(TESTMAT).tex
	make4ht $(TESTMAT).tex -c ml_make4ht -l -u -s -d $(MATHJAXDIRM) $(MATHJAXM)

testmatxm: $(TESTMAT).tex
	make4ht $(TESTMAT).tex -c ml_make4ht -l -u -s -d $(MATHMLDIRM) $(MATHMLM)

testmatj: $(TESTMAT).tex
	make4ht $(TESTMAT).tex -c ml_make4ht -l -u -s -d $(MATHJAXDIR) $(MATHJAX)

testmatx: $(TESTMAT).tex
	make4ht $(TESTMAT).tex -c ml_make4ht -l -u -s -d $(MATHMLDIR) $(MATHML)

all: matlibera \
testcls testclsj testclsx \
testgraf testgrafj testgrafx \
testmat testmatj testmatx \
clean

clean:
	@$(RM) $(FILE_CLEAN)

cleanall: clean
	@$(RM) $(NAMECLS).cls $(NAMEMAT).sty $(NAMEGRAF).sty *.pdf

dist-tar: clean
	rm -f $(TAR)
	tar --exclude '*.zip' -zcvf $(TAR) *

dist-zip: clean
	rm -f $(ZIP)
	zip -r $(ZIP) . -x '*.tar.gz'
